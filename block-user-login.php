<?php
/*
Plugin Name: Block User Login
Version: 0.1
Description: If enabled, blocks Webcast Users with role <strong>Subscriber</strong> from login. Notice that the testing user <strong>johndoe</strong> is set with <strong>Contributor</strong> role, so this plugin doesn't apply him and he's good to login normally. This plugin is disabled on the day of the webcast so login is allowed to general public (role <strong>Subscriber</strong>).
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/block-user-login
Text Domain: block-user-login
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// Temporary Block Webcast Users from login
add_filter( 'authenticate', 'bul_block_webcast_users', 30, 3 );
function bul_block_webcast_users( $user, $username, $password ) {
	if ($user instanceof WP_User && user_can($user, 'subscriber')) {
		return new WP_Error(1, 'Login Temporarily Disabled');
	}
	return $user;
}
